// Using an object literal for a jQuery feature
var clothSwitcher = {

    init: function( settings ) {
        clothSwitcher.config = {
            currentTopStyle: null,
            currentBottomStyle: null,
            currentBeltStyle: null,
            currentPrintStyle: null,

            // step one
            topSelector: $('.jcarousel-wrapper .jcarousel ul#topStyle li'),
            bottomSelector: $('.jcarousel-wrapper .jcarousel ul#bottomStyle li'),

            // step two
            printSelector: $('ul.printStype li'),

            // step three
            colorSelector: $('ul.colorStyle li'),

            // steps action
            actionButton: $('.switcher-actions-next'),
            stepsContainer: $('.steps'),

            // preview
            topImage: $('.swicher-preview .top img'),
            beltImage: $('.swicher-preview .belt img'),
            bottomImage: $('.swicher-preview .bottom img')
        };
 
        // Allow overriding the default config
        $.extend( clothSwitcher.config, settings );
 
        clothSwitcher.setup();
    },

    setup: function() {
        // step 1 - top
        clothSwitcher.config.topSelector.click( clothSwitcher.topSelectorClicked );

        // step 1 - bottom
        clothSwitcher.config.bottomSelector.click( clothSwitcher.bottomSelectorClicked );

        // step 2
        clothSwitcher.config.printSelector.click( clothSwitcher.printSelectorClicked );

        // step 3
        clothSwitcher.config.colorSelector.click( clothSwitcher.beltColorSelectorClicked );

        clothSwitcher.config.actionButton.on('click', function(e) {
            clothSwitcher.config.stepsContainer.removeClass('active');
            $('#step-' + $(this).attr('id')).addClass('active');
            if ($(this).attr('id') === '01') {
                clothSwitcher.setDefault();
            }
        });

        clothSwitcher.setDefault();
    },

    // bottom style option - step 1 - top
    topSelectorClicked: function() {
        var selectedImage = $(this).find('img');
        var selectedTopStyle = {
            img: selectedImage.attr('src'),
            id: selectedImage.attr('id')
        }
        if (selectedTopStyle) {
            clothSwitcher.setTopStyle(selectedTopStyle);
            clothSwitcher.populateStyle();
        }
    },

    setTopStyle: function(selectedTopStyle) {
        clothSwitcher.config.currentTopStyle = selectedTopStyle;
    },

    getTopStyle: function () {
        return clothSwitcher.config.currentTopStyle;
    },

    // bottom style option - step 1 - bottom
    bottomSelectorClicked: function() {
        var selectedImage = $(this).find('img');
        var bottomStyleImage = {
            img: selectedImage.attr('src'),
            id: selectedImage.attr('id')
        }
        if (bottomStyleImage) {
            clothSwitcher.setBottomStyle(bottomStyleImage);
            clothSwitcher.populateStyle();
        }
    },

    setBottomStyle: function(selectedBottomStyle) {
        clothSwitcher.config.currentBottomStyle = selectedBottomStyle;
    },

    getBottomStyle: function () {
        return clothSwitcher.config.currentBottomStyle;
    },

    // print style options - step 2
    printSelectorClicked: function() {
        var selectedImage = $(this).find('img');
        var selectedPrintStyle = {
            img: selectedImage.attr('src'),
            id: selectedImage.attr('id')
        }
        if (selectedPrintStyle) {
            clothSwitcher.setPrintStyle(selectedPrintStyle);
            clothSwitcher.populateStyle();
        }
    },

    setPrintStyle: function(selectedPrintStyle) {
        clothSwitcher.config.currentPrintStyle = selectedPrintStyle;
    },

    getPrintStyle: function () {
        return clothSwitcher.config.currentPrintStyle;
    },

    // belt color options - stpe 3
    beltColorSelectorClicked: function() {
        var selectedImage = $(this).find('img');
        var selectedBeltColorStyle = {
            img: selectedImage.attr('src'),
            id: selectedImage.attr('id')
        }
        if (selectedBeltColorStyle) {
            clothSwitcher.setBeltColorStyle(selectedBeltColorStyle);
            clothSwitcher.populateStyle();
        }
    },

    setBeltColorStyle: function(selectedBeltColorStyle) {
        clothSwitcher.config.currentBeltStyle = selectedBeltColorStyle;
    },

    getBeltColorStyle: function () {
        return clothSwitcher.config.currentBeltStyle;
    },
    
    // for reset functionality to set images in default
    setDefault: function() {
        clothSwitcher.setTopStyle({
            img : 'https://via.placeholder.com/350?text=Top'
        });
        clothSwitcher.setBottomStyle({
            img : 'https://via.placeholder.com/350?text=Bottom'
        });
        clothSwitcher.setBeltColorStyle({
            img : 'https://via.placeholder.com/350X50?text=belt'
        });
        clothSwitcher.setPrintStyle(null);
        clothSwitcher.populateStyle();
    },

    // populate selected styles
    populateStyle: function() {
        // set var for initial state
        var topImage = clothSwitcher.getTopStyle().img;
        var bottomImage = clothSwitcher.getBottomStyle().img;
        var beltImage = clothSwitcher.getBeltColorStyle().img;

        // override the top image
        if (clothSwitcher.getPrintStyle()) {
            topImage = clothSwitcher.getPrintStyle().img + '-' + clothSwitcher.getTopStyle().id;    
        }

        // override the bottom image
        if (clothSwitcher.getPrintStyle()) {
            bottomImage = clothSwitcher.getPrintStyle().img + '-' + clothSwitcher.getBottomStyle().id;    
        }

        // populate new set on image style
        clothSwitcher.config.topImage.attr('src', topImage);
        clothSwitcher.config.bottomImage.attr('src', bottomImage);
        clothSwitcher.config.beltImage.attr('src', beltImage);
    }
};
 
$( document ).ready(
    clothSwitcher.init
);